from django.shortcuts import render

from robot.cuerpo.tripioCuadrado import Tripio

miTripio = Tripio("Camy")

def index(request):
    return render(request, './robot/index.html')


def brazo_mover(request):
    miTripio.brazo_mover()
    return render(request, './robot/index.html')

def brazo_arriba(request):
    miTripio.brazo_arriba()
    return render(request, './robot/index.html')

def brazo_abajo(request):
    miTripio.brazo_abajo()
    return render(request, './robot/index.html')

def bailar(request):
    miTripio.bailar()
    return render(request, './robot/index.html')
