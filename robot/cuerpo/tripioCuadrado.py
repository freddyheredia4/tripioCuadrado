import time
from .brazo import Brazo


class Tripio:

    def __init__(self, nombre):
        self.nombre = nombre
        self.brazo_izquierdo = Brazo(17)
        self.brazo_derecho = Brazo(18)

    def brazo_mover(self):
        self.brazo_izquierdo.mover()
    def brazo_arriba(self):
        self.brazo_izquierdo.arriba()
    def brazo_abajo(self):
        self.brazo_izquierdo.abajo()
    def bailar(self):
        for n in range(4):
            self.brazo_izquierdo.abajo()
            time.sleep(0.01)
            self.brazo_derecho.arriba()
            time.sleep(0.01)
            self.brazo_izquierdo.arriba()
            time.sleep(0.01)
            self.brazo_derecho.abajo()
