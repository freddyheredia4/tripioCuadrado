import RPi.GPIO as GPIO
import time
from gpiozero import Servo

class Brazo:

    def __init__(self, pin):
        print("Creado")
        self.servoPIN = pin
        self.local_gpio = GPIO
        self.local_gpio.setmode(GPIO.BCM)
        self.local_gpio.setup(self.servoPIN, GPIO.OUT)
        self.servo = self.local_gpio.PWM(self.servoPIN, 50)
    def reiniciar(self):
        print("Reiniciando")
        self.local_gpio.setmode(self.local_gpio.BCM)
        self.local_gpio.setup(self.servoPIN, self.local_gpio.OUT)
        time.sleep(1)
        self.servo.start(2)

    def detener(self):
        self.local_gpio.cleanup()


    def mover(self):
        print("moviendo brazo")
        self.reiniciar()
        for n in range(4):
            self.servo.ChangeDutyCycle(5)
            time.sleep(0.5)
            self.servo.ChangeDutyCycle(7.5)
            time.sleep(0.5)
            self.servo.ChangeDutyCycle(10)
            time.sleep(0.5)
            self.servo.ChangeDutyCycle(12)
            time.sleep(0.5)
            self.servo.ChangeDutyCycle(10)
            time.sleep(0.5)
            self.servo.ChangeDutyCycle(7.5)
            time.sleep(0.5)
            self.servo.ChangeDutyCycle(5)
            time.sleep(0.5)
            self.servo.ChangeDutyCycle(2)
            time.sleep(0.5)
        self.detener()
    def arriba(self):
        print("Arriba")
        self.reiniciar()
        self.servo.ChangeDutyCycle(11.5)
        time.sleep(0.5)
        self.servo.ChangeDutyCycle(12)
        self.detener()
    def abajo(self):
        print("Abajo")
        self.reiniciar()
        self.servo.ChangeDutyCycle(2)
        time.sleep(0.5)
        self.servo.ChangeDutyCycle(2.5)
        self.detener()
